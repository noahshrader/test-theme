/**
 * Slice theme scripts
 *
 * @package Slice
 * @author  LayerCakeCo
 * @license GPL-2.0-or-later
 */

( function( window, $, undefined ) {
  'use strict';

  // Mobile menu toggle interactions
  var isActive = false;
  $('.mobile-menu-toggle').on('click', function() {
    if (isActive) {
      $(this).removeClass('active');
      $('body').removeClass('menu-open');
    } else {
      $(this).addClass('active');
      $('body').addClass('menu-open');
    }
    isActive = !isActive;
  });

})( this, jQuery );
