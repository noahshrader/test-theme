/**
 * Slice theme scripts
 *
 * @package Slice
 * @author  LayerCakeCo
 * @license GPL-2.0-or-later
 */
!function(e,s,o){"use strict";var n=!1;s(".mobile-menu-toggle").on("click",function(){n?(s(this).removeClass("active"),s("body").removeClass("menu-open")):(s(this).addClass("active"),s("body").addClass("menu-open")),n=!n})}(0,jQuery);