<?php
/**
 * Slice
 *
 * This file adds the required CSS to the front end to Slice
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://layercakeco.com
 */

add_action('init', 'display_navigation_search');
/**
 * Display search in navigation
 *
 * @since 1.0.0
 */
function display_navigation_search() {
  $options = get_option( 'navigation_search' );
  if( $options === '1' ) {
    add_action('init', 'theme_menu_extras');
    function theme_menu_extras( $menu, $args ) {
      if ( 'primary' !== $args->theme_location )
        return $menu;
      $menu  .= '<div class="search-box">' . get_search_form( false ) . '</div>';
      return $menu;
    }
    add_filter( 'wp_nav_menu_items', 'theme_menu_extras', 10, 2 );
  }
}

$secondary_menu_placement_option = get_option( 'secondary_menu_placement' );
if ( $secondary_menu_placement_option === 'top' || $secondary_menu_placement_option === '' ) {
  add_action( 'genesis_before_header', 'place_secondary_menu', 20 );
} else {
  add_action( 'genesis_after_header', 'place_secondary_menu', 20 );
}
/**
 * Secondary menu placement
 *
 * @since 1.0.0
 */
function place_secondary_menu() {
  $args = array(
  'theme_location' => 'secondary-nav',
  'menu_class' => 'menu genesis-nav-menu menu-secondary responsive-menu',
  'container_class' => 'wrap'
  );

  if ( has_nav_menu( 'secondary-nav' ) ) {
    echo '<nav class="nav-secondary">';
    wp_nav_menu( $args );
    echo '</nav>';
  }
}
