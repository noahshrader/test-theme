<?php
/**
 * Slice
 *
 * This file adds the Customizer additions to Slice
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://layercakeco.com
 */

add_action( 'customize_register', 'slice_customizer_register' );
/**
 * Registers settings and controls with the Customizer.
 *
 * @since 2.2.3
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function slice_customizer_register( $wp_customize ) {
	$wp_customize->add_setting(
		'slice_link_color',
		array(
			'default'           => slice_customizer_get_default_link_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'slice_link_color',
			array(
				'description' => __( 'Change the color of post info links, hover color of linked titles, hover color of menu items, and more.', 'slice' ),
				'label'       => __( 'Link Color', 'slice' ),
				'section'     => 'colors',
				'settings'    => 'slice_link_color',
			)
		)
	);

	$wp_customize->add_setting(
		'slice_accent_color',
		array(
			'default'           => slice_customizer_get_default_accent_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'slice_accent_color',
			array(
				'description' => __( 'Change the default hover color for button links, the menu button, and submit buttons. This setting does not apply to buttons created with the Buttons block.', 'slice' ),
				'label'       => __( 'Accent Color', 'slice' ),
				'section'     => 'colors',
				'settings'    => 'slice_accent_color',
			)
		)
	);

	$wp_customize->add_setting(
		'slice_logo_width',
		array(
			'default'           => 300,
			'sanitize_callback' => 'absint',
		)
	);

	// Add a control for the logo size.
	$wp_customize->add_control(
		'slice_logo_width',
		array(
			'label'       => __( 'Logo Width', 'slice' ),
			'description' => __( 'The maximum width of the logo in pixels.', 'slice' ),
      'priority'    => 9,
			'section'     => 'title_tagline',
			'settings'    => 'slice_logo_width',
			'type'        => 'number',
			'input_attrs' => array(
				'min' => 100,
			),

		)
	);
}
