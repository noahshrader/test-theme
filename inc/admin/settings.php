<?php
/**
 * Admin
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://www.layercakeco.com
 */

defined( 'WPINC' ) || die;

add_action( 'admin_menu', 'slice_settings_admin_menu', 0 );
/**
 * Add the theme dashboard to the main WordPress dashboard menu.
 *
 * @since   3.1.0
 * @access  public
 * @return  void
 */
function slice_settings_admin_menu() {
	add_menu_page(
		'Slice Settings',
		'Slice Settings',
		'edit_theme_options',
		'slice-settings',
		'slice_settings_page',
		'58.998'
	);
}

/**
 * Include the base template for the settings page.
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function slice_settings_page() {
	require_once SLICE_THEME_ROOT . '/inc/admin/views/settings.php';
}

add_action( 'admin_enqueue_scripts', 'slice_settings_scripts', 10 );
/**
 * Load scripts and styles for the dashboard.
 *
 * @since   1.0.0
 * @access  public
 * @param   object $screen The current screen object.
 * @return  void
 */
function slice_settings_scripts( $screen ) {
	if ( 'toplevel_page_slice-settings' === $screen ) {
		wp_enqueue_style(
			'slice-settings',
			SLICE_THEME_URI . '/assets/css/settings.css',
			array(),
			CHILD_THEME_VERSION
		);
	}
}
