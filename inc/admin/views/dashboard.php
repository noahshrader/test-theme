<?php
/**
 * Dashboard View
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://www.layercakeco.com
 */

?>

<div class="slice-dashboard">

	<h1>Slice</h1>

	<section class="slice-dashboard-content">
		
		<ul>
			<li><?php echo "Slice Version: ".CHILD_THEME_VERSION; ?></li>
			<li><?php echo "Genesis Version: ".PARENT_THEME_VERSION; ?></li>
			<li><?php echo "Wordpress Version: ".get_bloginfo('version'); ?></li>
			<li><?php echo "PHP Version: ".phpversion(); ?></li>
		</ul>
		
	</section>

</div>
