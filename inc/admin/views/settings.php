<?php
ob_start();

add_action("admin_menu", "add_slice_settings_menu_item");
/**
 * Enqueue Add settings to Slice menu
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function add_slice_settings_menu_item() {
  add_submenu_page(
    'slice-dashboard',
    'Slice Settings',
    'Slice Settings',
    'manage_options',
    'slice-settings',
    'slice_settings_view',
    null,
    99
  );
}

// Enable navigation search
function enable_navigation_search() {
?>
  <input type="checkbox" name="navigation_search" value="1" <?php checked(1, get_option('navigation_search'), true); ?> />
<?php
}

// Placement of secondary menu
function secondary_menu_placement() {
?>
  <select name="secondary_menu_placement">
    <option value="top" <?php selected(get_option('secondary_menu_placement'), "top"); ?>>Top</option>
    <option value="bottom" <?php selected(get_option('secondary_menu_placement'), "bottom"); ?>>Bottom</option>
  </select>
<?php
}

add_action("admin_init", "display_slice_settings_fields");
/**
 * Enqueue Register Client Portal options
 *
 * @since   1.0.0
 * @access  public
 * @return  void
 */
function display_slice_settings_fields() {
  add_settings_section("section", "Main Settings", null, "slice-settings-options");
  add_settings_field("navigation_search", "Enable Search in Navigation", "enable_navigation_search", "slice-settings-options", "section");
  add_settings_field("secondary_menu_placement", "Placement of Secondary Navigation", "secondary_menu_placement", "slice-settings-options", "section");
  register_setting("section", "navigation_search");
  register_setting("section", "secondary_menu_placement");
}

// Display settings
function slice_settings_view() {
?>
<div class="wrap">
  <h1>Client Portal Settings</h1>
  <form method="post" action="options.php">
    <?php
      settings_fields("section");
      do_settings_sections("slice-settings-options");
      submit_button();
    ?>
  </form>
</div>
<?php
}
?>