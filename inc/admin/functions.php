<?php
/**
 * Admin
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://www.layercakeco.com
 */

defined( 'WPINC' ) || die;

if ( (bool) apply_filters( 'slice_enable_theme_dashboard', true ) ) {
	require_once SLICE_THEME_ROOT . '/inc/admin/dashboard.php';
}

// Settings
include(SLICE_THEME_ROOT . '/inc/admin/views/settings.php');

add_action( 'admin_enqueue_scripts', 'slice_load_admin_styles' );
function slice_load_admin_styles() {
	wp_enqueue_style(
		'slice-admin',
		SLICE_THEME_URI . '/assets/css/admin.css',
		array(),
		CHILD_THEME_VERSION
	);
}
