/**
 * Trigger AJAX request to save state when the WooCommerce notice is dismissed.
 *
 * @version 2.3.0
 *
 * @author LayerCakeCo
 * @license GPL-2.0-or-later
 * @package Slice
 */

jQuery( document ).on(
	'click', '.slice-woocommerce-notice .notice-dismiss', function() {

		jQuery.ajax(
			{
				url: ajaxurl,
				data: {
					action: 'slice_dismiss_woocommerce_notice'
				}
			}
		);

	}
);
