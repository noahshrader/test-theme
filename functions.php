<?php
/**
 * Slice
 *
 * Main functions
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://www.layercakeco.com
 */

// Start the engine
require_once get_template_directory() . '/lib/init.php';

// Constants
define( 'CHILD_THEME_HANDLE', sanitize_title_with_dashes( wp_get_theme()->get( 'Name' ) ) );
define( 'CHILD_THEME_VERSION', wp_get_theme()->get( 'Version' ) );
define( 'SLICE_THEME_ROOT', trailingslashit( get_stylesheet_directory() ) );
define( 'SLICE_THEME_URI', trailingslashit( get_stylesheet_directory_uri() ) );

// Load defaults
require_once SLICE_THEME_ROOT . '/inc/theme-defaults.php';

// Load admin functions
if ( is_admin() ) {
	require_once SLICE_THEME_ROOT . '/inc/admin/functions.php';
}

add_action( 'after_setup_theme', 'slice_localization' );
/**
 * Load localization
 *
 * @since 1.0.0
 */
function slice_localization() {
	load_child_theme_textdomain( 'slice', SLICE_THEME_ROOT . '/languages' );
}

// Base theme settings
require_once SLICE_THEME_ROOT . '/inc/slice.php';

// Helper functions
require_once SLICE_THEME_ROOT . '/inc/helper-functions.php';

// Customizer settings
require_once SLICE_THEME_ROOT . '/inc/customize.php';

// Customizer CSS
require_once SLICE_THEME_ROOT . '/inc/output.php';

// WooCommerce support
require_once SLICE_THEME_ROOT . '/inc/woocommerce/woocommerce-setup.php';

// WooCommerce styles and Customizer CSS
require_once SLICE_THEME_ROOT . '/inc/woocommerce/woocommerce-output.php';

// Genesis Connect WooCommerce notice
require_once SLICE_THEME_ROOT . '/inc/woocommerce/woocommerce-notice.php';

// Include theme updater
require SLICE_THEME_ROOT . 'inc/updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://github.com/noahshrader/layercake-slice/',
	__FILE__,
	'slice'
);
$myUpdateChecker->setAuthentication('55577ab199810950e6e15d37c1195880556de1e3');

add_action( 'after_setup_theme', 'gutenberg_support' );
/**
 * Gutenberg support
 *
 * @since 1.0.0
 */
function gutenberg_support() {
	require_once SLICE_THEME_ROOT . '/inc/gutenberg/init.php';
}

add_action( 'wp_enqueue_scripts', 'slice_enqueue_scripts_styles' );
/**
 * Enqueue styles and scripts
 *
 * @since 1.0.0
 */
function slice_enqueue_scripts_styles() {
	wp_enqueue_style(
		'slice-fonts',
		'//fonts.googleapis.com/css?family=Muli:400,700',
		array(),
		CHILD_THEME_VERSION
	);
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_script(
		'slice',
		get_stylesheet_directory_uri() . '/assets/js/min/slice.js',
		array( 'jquery' ),
		CHILD_THEME_VERSION,
		true
	);
}

// HTML5 markup support
add_theme_support( 'html5', genesis_get_config( 'html5' ) );

// Accessibility support
add_theme_support( 'genesis-accessibility', genesis_get_config( 'accessibility' ) );

// Viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

// Custom logo in Customizer > Site Identity
add_theme_support( 'custom-logo', genesis_get_config( 'custom-logo' ) );

// Rename primary and secondary navigation menus
add_theme_support( 'genesis-menus', genesis_get_config( 'menus' ) );

// Image sizes
add_image_size( 'sidebar-featured', 80, 80, true );

// After entry widget support
add_theme_support( 'genesis-after-entry-widget-area' );

// 3-column footer widget support
add_theme_support( 'genesis-footer-widgets', 3 );

// Remove header right widget area
unregister_sidebar( 'header-right' );

// Remove secondary sidebar
unregister_sidebar( 'sidebar-alt' );

// Add widget area for the recipe index
genesis_register_sidebar( array(
	'id'          => 'recipe-index',
	'name'        => __( 'Recipe Index', 'slice' ),
	'description' => __( 'This is the basis of the recipe index.', 'slice' ),
) );

// Remove unnecessary site layouts
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

// Remove output of primary navigation right extras
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

add_action( 'genesis_theme_settings_metaboxes', 'slice_remove_metaboxes' );
/**
 * Remove output of unused admin settings metaboxes
 *
 * @since 1.0.0
 *
 * @param string $_genesis_admin_settings The admin screen to remove meta boxes from.
 */
function slice_remove_metaboxes( $_genesis_admin_settings ) {
	remove_meta_box( 'genesis-theme-settings-header', $_genesis_admin_settings, 'main' );
	remove_meta_box( 'genesis-theme-settings-nav', $_genesis_admin_settings, 'main' );
}

add_filter( 'genesis_customizer_theme_settings_config', 'slice_remove_customizer_settings' );
/**
 * Remove breadcrumbs on home page
 *
 * @since 1.0.0
 *
 * @param array $config Original Customizer items.
 * @return array Filtered Customizer items.
 */
function slice_remove_customizer_settings( $config ) {
	unset( $config['genesis']['sections']['genesis_header'] );
	unset( $config['genesis']['sections']['genesis_breadcrumbs']['controls']['breadcrumb_front_page'] );
	return $config;
}

// Allow custom logo
add_action( 'genesis_site_title', 'the_custom_logo', 0 );

// Relocate primary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

// Relocate the footer navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 10 );

// Add Secondary menu
function slice_secondary_nav() {
  $locations = array(
    'secondary-nav' => __( 'Secondary Menu' ),
  );
  register_nav_menus( $locations );
}
add_action( 'init', 'slice_secondary_nav' );

add_filter( 'get_search_form', 'slice_search_placeholder' );
/**
 * Replace placeholder in main search
 *
 * @since  1.0.0
 * @access public
 * @return string
 */
function slice_search_placeholder( $html ) {
	$html = str_replace( 'placeholder="Search this website', 'placeholder="Search...', $html );
	return $html;
}

add_filter( 'genesis_search_button_text', 'slice_search_button_text' );
/**
 * Customize search button text
 *
 * @since  1.0.0
 * @access public
 * @return string
 */
function slice_search_button_text() {
	return '&#xf179;';
}

add_filter( 'wp_nav_menu_args', 'slice_secondary_menu_args' );
/**
 * Force secondary menu to one level
 *
 * @since 1.0.0
 *
 * @param array $args Original menu options.
 * @return array Menu options with depth set to 1.
 */
function slice_secondary_menu_args( $args ) {
	if ( 'secondary' !== $args['theme_location'] ) {
		return $args;
	}
	$args['depth'] = 1;
	return $args;
}

add_filter( 'genesis_author_box_gravatar_size', 'slice_author_box_gravatar' );
/**
 * Author box Gravatar size
 *
 * @since 1.0.0
 *
 * @param int $size Original icon size.
 * @return int Modified icon size.
 */
function slice_author_box_gravatar( $size ) {
	return 80;
}

add_filter( 'genesis_comment_list_args', 'slice_comments_gravatar' );
/**
 * Comments Gravatar size
 *
 * @since 1.0.0
 *
 * @param array $args Gravatar settings.
 * @return array Gravatar settings with modified size.
 */
function slice_comments_gravatar( $args ) {
	$args['avatar_size'] = 65;
	return $args;
}

// Hide site title and description if logo is present
if (has_custom_logo()) {
  remove_action( 'genesis_site_title', 'genesis_seo_site_title' );
  remove_action( 'genesis_site_description', 'genesis_seo_site_description' );
}

// Remove 'You Are Here' from breadcrumb menu
add_filter( 'genesis_breadcrumb_args', 'afn_breadcrumb_args' );
function afn_breadcrumb_args( $args ) {
	$args['labels']['prefix'] = '';
  return $args;
}

add_filter( 'genesis_do_nav', 'mobile_toggle', 10, 2 );
/**
 * Inserts mobile toggle after primary navigation container
 *
 * @since 1.0.0
 *
 * @param string $close_html HTML tag being processed by the API.
 * @param array  $args Array with markup arguments.
 *
 * @return string
 */
function mobile_toggle( $close_html, $args ) {
  if ( $close_html ) {
    $add_html = '<a class="mobile-menu-toggle"><span></span></a>';
    $close_html = $close_html . $add_html;
  }
  return $close_html;
}
