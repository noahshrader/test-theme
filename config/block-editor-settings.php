<?php
/**
 * Block Editor settings specific to Slice
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://layercakeco.com
 */

$slice_link_color            = get_theme_mod( 'slice_link_color', slice_customizer_get_default_link_color() );
$slice_link_color_contrast   = slice_color_contrast( $slice_link_color );
$slice_link_color_brightness = slice_color_brightness( $slice_link_color, 35 );

return array(
	'admin-fonts-url'              => 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,700',
	'content-width'                => 1062,
	'default-button-bg'            => $slice_link_color,
	'default-button-color'         => $slice_link_color_contrast,
	'default-button-outline-hover' => $slice_link_color_brightness,
	'default-link-color'           => $slice_link_color,
	'editor-color-palette'         => array(
		array(
			'name'  => __( 'Custom color', 'slice' ), // Called “Link Color” in the Customizer options. Renamed because “Link Color” implies it can only be used for links.
			'slug'  => 'theme-primary',
			'color' => get_theme_mod( 'slice_link_color', slice_customizer_get_default_link_color() ),
		),
		array(
			'name'  => __( 'Accent color', 'slice' ),
			'slug'  => 'theme-secondary',
			'color' => get_theme_mod( 'slice_accent_color', slice_customizer_get_default_accent_color() ),
		),
	),
	'editor-font-sizes'            => array(
		array(
			'name' => __( 'Small', 'slice' ),
			'size' => 12,
			'slug' => 'small',
		),
		array(
			'name' => __( 'Normal', 'slice' ),
			'size' => 18,
			'slug' => 'normal',
		),
		array(
			'name' => __( 'Large', 'slice' ),
			'size' => 20,
			'slug' => 'large',
		),
		array(
			'name' => __( 'Larger', 'slice' ),
			'size' => 24,
			'slug' => 'larger',
		),
	),
);
