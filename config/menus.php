<?php
/**
 * Slice
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://layercakeco.com
 */

/**
 * Supported Genesis navigation menus.
 */
return array(
	'primary'   => __( 'Header Menu', 'slice' ),
	'secondary' => __( 'Footer Menu', 'slice' ),
);
