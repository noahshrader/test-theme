<?php
/**
 * Slice
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://layercakeco.com
 */

/**
 * Genesis Accessibility features to support.
 */
return array(
	'404-page',
	'drop-down-menu',
	'headings',
	'search-form',
	'skip-links',
);
