<?php
/**
 * Slice Simple Social Icons default settings
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://layercakeco.com
 */

return array(
	'alignment'              => 'alignleft',
	'background_color'       => '#f5f5f5',
	'background_color_hover' => '#333333',
	'border_radius'          => 3,
	'border_width'           => 0,
	'icon_color'             => '#333333',
	'icon_color_hover'       => '#ffffff',
	'size'                   => 40,
);
