<?php
/**
 * Genesis Slice child theme.
 *
 * @package Slice
 * @author  Layer Cake Co.
 * @license GPL-2.0-or-later
 * @link    https://layercakeco.com
 */

/**
 * Custom Logo configuration.
 */
return array(
	'height'      => 120,
	'width'       => 700,
	'flex-height' => true,
	'flex-width'  => true,
);
